import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblWidth;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STTblWidth;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ReportGenerator {

    private XWPFDocument document = new XWPFDocument();

    private Iterator<String> fileIterator;

    private XWPFTable table;

    private static final String FOLDER_NAME = "miniatury";

    private static final String OUTPUT_FILE_NAME = "sprawozdanie.docx";

    private Integer rowNumber;

    private final Integer columnNumber = 13;

    private ReportGenerator() {
        List<String> files = Arrays.stream(Objects.requireNonNull(new File("miniatury").list()))
                .sorted()
                .collect(Collectors.toList());

        if(files.isEmpty()) {
            throw new IllegalArgumentException("Folder z miniaturami jest pusty");
        }

        fileIterator = files.iterator();

        rowNumber = files.size() / columnNumber;
    }

    private void createReport() throws IOException, InvalidFormatException {
        createTable();
        fillHeader();
        fillTable();
    }

    private void createTable() {
        table = document.createTable();
        CTTblWidth width = table.getCTTbl().addNewTblPr().addNewTblW();
        width.setType(STTblWidth.DXA);
        width.setW(BigInteger.valueOf(8555));

        for(int i = 1; i < columnNumber; ++i) {
            table.getRow(0).createCell();
        }

        for(int i = 0; i < rowNumber; ++i) {
            table.createRow();
        }
    }

    private void fillHeader() {
        XWPFTableRow readRow = table.getRow(0);
        readRow.getCell(0).setText("Obrazy inicjujące");

        String[] readText = {"OdczytMAL", "Odczyt MAC", "Odczyt MS"};

        for(int i = 1; i < 13; ++i) {
            readRow.getCell(i).setText(readText[i % 3]);
        }
    }

    private void fillTable() throws InvalidFormatException, IOException {
        FileOutputStream out = new FileOutputStream(new File(OUTPUT_FILE_NAME));

        for(int i = 0; i < columnNumber; ++i){
            for(int j = 1; j < rowNumber + 1; ++j) {
                XWPFTableCell cell = table.getRow(j).getCell(i);
                FileInputStream fis = new FileInputStream(getNextFile());
                XWPFParagraph paragraph = cell.addParagraph();
                XWPFRun run = paragraph.createRun();
                run.addPicture(fis, XWPFDocument.PICTURE_TYPE_BMP, "miniature", Units.toEMU(16),
                        Units.toEMU(20));
            }
        }

        document.write(out);
        out.close();
    }

    private String getNextFile() {
        return fileIterator.hasNext() ? FOLDER_NAME + File.separator + fileIterator.next() : "";
    }

    public static void main(String[] args) throws IOException, InvalidFormatException {
        ReportGenerator reportGenerator = new ReportGenerator();
        reportGenerator.createReport();
    }

}
